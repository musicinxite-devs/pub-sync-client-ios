//
//  Offline_Library+CoreDataProperties.swift
//  
//
//  Created by Kishor Lodhia on 01/10/16.
//
//

import Foundation
import CoreData

extension Offline_Library {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Offline_Library> {
        return NSFetchRequest<Offline_Library>(entityName: "Offline_Library");
    }

    @NSManaged public var id: NSNumber
    @NSManaged public var status: String?
    @NSManaged public var title: String?
    @NSManaged public var streams: NSSet?
    @NSManaged public var tracks: NSSet?

}

// MARK: Generated accessors for streams
extension Offline_Library {

    @objc(addStreamsObject:)
    @NSManaged public func addToStreams(_ value: Streams)

    @objc(removeStreamsObject:)
    @NSManaged public func removeFromStreams(_ value: Streams)

    @objc(addStreams:)
    @NSManaged public func addToStreams(_ values: NSSet)

    @objc(removeStreams:)
    @NSManaged public func removeFromStreams(_ values: NSSet)

}

// MARK: Generated accessors for tracks
extension Offline_Library {

    @objc(addTracksObject:)
    @NSManaged public func addToTracks(_ value: Tracks)

    @objc(removeTracksObject:)
    @NSManaged public func removeFromTracks(_ value: Tracks)

    @objc(addTracks:)
    @NSManaged public func addToTracks(_ values: NSSet)

    @objc(removeTracks:)
    @NSManaged public func removeFromTracks(_ values: NSSet)

}
