//
//  Streams+CoreDataProperties.swift
//  
//
//  Created by Kishor Lodhia on 01/10/16.
//
//

import Foundation
import CoreData

extension Streams {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Streams> {
        return NSFetchRequest<Streams>(entityName: "Streams");
    }

    @NSManaged public var id: NSNumber
    @NSManaged public var status: String?
    @NSManaged public var title: String?
    @NSManaged public var url: String?
    @NSManaged public var updated_at: String?
    @NSManaged public var offline: Offline_Library?

}
