//
//  Tracks+CoreDataProperties.swift
//  
//
//  Created by Kishor Lodhia on 01/10/16.
//
//

import Foundation
import CoreData

extension Tracks {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Tracks> {
        return NSFetchRequest<Tracks>(entityName: "Tracks");
    }

    @NSManaged public var id: NSNumber
    @NSManaged public var path: String?
    @NSManaged public var title: String?
    @NSManaged public var updated_at: String?
    @NSManaged public var offline_library: NSSet?

}

// MARK: Generated accessors for offline_library
extension Tracks {

    @objc(addOffline_libraryObject:)
    @NSManaged public func addToOffline_library(_ value: Offline_Library)

    @objc(removeOffline_libraryObject:)
    @NSManaged public func removeFromOffline_library(_ value: Offline_Library)

    @objc(addOffline_library:)
    @NSManaged public func addToOffline_library(_ values: NSSet)

    @objc(removeOffline_library:)
    @NSManaged public func removeFromOffline_library(_ values: NSSet)

}
