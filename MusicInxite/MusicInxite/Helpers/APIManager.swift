//
//  APIManager.swift
//  MusicInxite
//
//  Created by Kishor Lodhia on 08/10/16.
//  Copyright © 2016 Kishor Lodhia. All rights reserved.
//

import Foundation

import Alamofire
//import SwiftyJSON

import UIKit

class APIManager: NSObject {
    
    static let sharedInstance = APIManager()
    
    var Manager: SessionManager!
    let serverTrustPolicies: [String: ServerTrustPolicy] = [
        
        "playsync.musicinxite.com": .disableEvaluation
    ]
    override init() {
    Manager = SessionManager(
        serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
    )
    }
    
    // MARK:- Login WebServices
    func login(user_email:String,Password:String,completion:@escaping (_ dict:[String:AnyObject],_ error:NSError?)->Void) {
        
        let DeviceIdentifier = UIDevice.current.identifierForVendor?.uuidString
        let parameters = ["email":user_email,"password":Password,"uu_id":DeviceIdentifier]
        
        Alamofire.request(Login_API, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).validate().responseJSON { (response: DataResponse<Any>) in
            
            switch(response.result) {
                
            case .success(_):
                if response.result.value != nil{
                    //print(response.result.value!)
                    
                    var dict = [String:AnyObject]()
                    //dict["data"] = response.result.value as AnyObject?
                    dict = response.result.value as! [String:AnyObject]
                    completion(dict,nil)
                }
                break
                
            case .failure(_):
                print(response.result.error as Any)
                completion([:],response.result.error as NSError?)
                break
                
            }
        }
        
        
    }

    
    //MARK:- FORGOT Password
    func forgot_password(user_email:String,completion:@escaping (_ dict:[String:AnyObject],_ error:NSError?)->Void) {
        
        
        let parameters = ["email":user_email]
        
        Alamofire.request(ForogotPassword_API, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).validate().responseJSON { (response: DataResponse<Any>) in
            
            switch(response.result) {
                
            case .success(_):
                if response.result.value != nil{
                    //print(response.result.value!)
                    
                    var dict = [String:AnyObject]()
                    //dict["data"] = response.result.value as AnyObject?
                    dict = response.result.value as! [String:AnyObject]
                    completion(dict,nil)
                }
                break
                
            case .failure(_):
                print(response.result.error as Any)
                completion([:],response.result.error as NSError?)
                break
                
            }
        }
    }
    
    //MARK:- Logout WebService
    func logout(completion:@escaping (_ dict:[String:AnyObject],_ error:NSError?)->Void) {
        
        let userEmail = UserDefaults.standard.value(forKey: "User_Email") as! String?
        let device_id = UIDevice.current.identifierForVendor?.uuidString
        let parameters = ["email":userEmail,"uu_id":device_id]
        
        Alamofire.request(Logout_API, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).validate().responseJSON { (response: DataResponse<Any>) in
            
            switch(response.result) {
                
            case .success(_):
                if response.result.value != nil{
                    //print(response.result.value!)
                    
                    var dict = [String:AnyObject]()
                    //dict["data"] = response.result.value as AnyObject?
                    dict = response.result.value as! [String:AnyObject]
                    completion(dict,nil)
                }
                break
                
            case .failure(_):
                print(response.result.error as Any)
                completion([:],response.result.error as NSError?)
                break
                
            }
        }
    }
    
    // MARK:- Activity Log WebServices
    func postActivity(eventName:String,eventTime:String,completion:@escaping (_ dict:[String:AnyObject],_ error:NSError?)->Void) {
        
        let DeviceIdentifier = UIDevice.current.identifierForVendor?.uuidString
        
        let dict = UserDefaults.standard.value(forKey: "User_Info") as! [String:AnyObject]
        let CustomerID = dict["customer_id"]!
        
        let systemVersion = UIDevice.current.systemVersion
        let modelName = UIDevice.current.model
        
        let parameters = ["event":eventName,"event_datetime":eventTime,"device_detail":DeviceIdentifier as Any,"customer_id":CustomerID, "model": modelName, "OSVersion":systemVersion] as [String : Any]
        
        Alamofire.request(ActivityLogs_API, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).validate().responseJSON { (response: DataResponse<Any>) in
            
            switch(response.result) {
                
            case .success(_):
                if response.result.value != nil{
                    
                    print("Log Activity: \(response.result.value!)")
                    //print(response.result.value!)
                    
                    var dict = [String:AnyObject]()
                    //dict["data"] = response.result.value as AnyObject?
                    dict = response.result.value as! [String:AnyObject]
                    completion(dict,nil)
                }
                break
                
            case .failure(_):
                print(response.result.error as Any)
                completion([:],response.result.error as NSError?)
                break
                
            }
        }
        
//        let parameters = ["event":eventName,"event_datetime":eventTime,"device_detail":DeviceIdentifier,"customer_id":CustomerID] as [String : Any]
        
    }
    
    // MARK: - Track Request API
    func trackRequest(stream_id:String,meta_data:String,type:String,completion:@escaping (_ dict:[String:AnyObject],_ error:NSError?)->Void) {
        
        let dict = UserDefaults.standard.value(forKey: "User_Info") as! [String:AnyObject]
        let CustomerID = dict["customer_id"]!
        let parameters = ["stream_id":stream_id,"customer_id":CustomerID,"meta_data":meta_data,"type":type] as [String : Any];
        
        Alamofire.request(TrackRequest_API, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).validate().responseJSON { (response: DataResponse<Any>) in
            
            //Google Analytics tracking
            let trackerParameters = ["stream_id":stream_id,"customer_id":CustomerID,"songname":meta_data,"type":type] as [String : Any];
            let tracker = GAI.sharedInstance().defaultTracker
            tracker?.send(trackerParameters)
            
            switch(response.result) {
                
            case .success(_):
                if response.result.value != nil {
                    //print(response.result.value!)
                    
                    var dict = [String:AnyObject]()
                    //dict["data"] = response.result.value as AnyObject?
                    dict = response.result.value as! [String:AnyObject]
                    completion(dict,nil)
                }
                break
                
            case .failure(_):
                print(response.result.error as Any)
                completion([:],response.result.error as NSError?)
                break
                
            }
        }
    }
    
}

