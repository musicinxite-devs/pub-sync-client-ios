//
//  APIs.swift
//  MusicInxite
//
//  Created by Kishor Lodhia on 08/10/16.
//  Copyright © 2016 Kishor Lodhia. All rights reserved.
//

import Foundation

#if DEV
let BASE_URL = "https://playsync.musicinxite.com"
//let BASE_URL = "http://192.168.5.3:8000"
#else
let BASE_URL = "https://playsync.musicinxite.com"
#endif

let Login_API = BASE_URL + "/api/authenticate"
let ForogotPassword_API = BASE_URL + "/api/forget-password"
let ActivityLogs_API = BASE_URL + "/api/log"
let Logout_API = BASE_URL + "/api/terminate"
let TrackRequest_API = BASE_URL + "/api/removal"
