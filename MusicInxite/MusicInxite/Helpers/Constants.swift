//
//  Constants.swift
//  MusicInxite
//
//  Created by Kishor Lodhia on 19/09/16.
//  Copyright © 2016 Kishor Lodhia. All rights reserved.
//

import Foundation
import UIKit


let appDelegate = UIApplication.shared.delegate as! AppDelegate

let documentsUrl = FileManager.default.urls(for: .documentDirectory,
                                            in: .userDomainMask).first!

let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)

struct CustomFonts {
    
   static func CaviarDreams(_ size:CGFloat) -> UIFont {
        return UIFont(name: "CaviarDreams", size: size)!
    }
}

struct  color {
    static func RGBCOLOR(_ red: Int, green: Int, blue: Int) -> UIColor {
        return UIColor(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1)
    }
    

}
