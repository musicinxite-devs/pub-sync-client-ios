//
//  DBAction.swift
//  MusicInxite
//
//  Created by Kishor Lodhia on 08/10/16.
//  Copyright © 2016 Kishor Lodhia. All rights reserved.
//

import UIKit

import CoreData

class DBAction: NSObject {

    static let sharedInstance = DBAction()
    var context = appDelegate.persistentContainer.viewContext
    
    
    //MARK: Check Remining Downloads
    func check_remaining_downloads(Entity:String,stream_id:Int,predicate_str:String,parameter:Any) -> (arr:Array<AnyObject>?,isExists:Bool,Error:NSError?) {
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: Entity)
//        let predicate = NSPredicate(format: "\(predicate_str)=%@", parameter as! CVarArg)
        let predicate = NSPredicate(format: "ANY offline_library.id == \(stream_id) AND path = %@", parameter as! CVarArg)
        
        fetchRequest.predicate=predicate
        
        do {

            let arr = try context.fetch(fetchRequest)
            if !arr.isEmpty{
                return(arr as Array<AnyObject>?,true,nil)
            }
            else{
                return(arr as Array<AnyObject>?,false,nil)
            }
            
            // success ...
        } catch let error as NSError {
            // failure
            print("Fetch failed: \(error.localizedDescription)")
            return(nil,true,error)
        }
        
        
    }
    
    //MARK: CheckDuplication
    func checkDuplication(Entity:String,predicate_str:String,ID:NSNumber) -> (arr:Array<AnyObject>?,isExists:Bool,Error:NSError?) {
        
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: Entity)
        let predicate = NSPredicate(format: "\(predicate_str)=%@", ID)
        
        fetchRequest.predicate=predicate
        
        do {
            let arr = try context.fetch(fetchRequest)
            if !arr.isEmpty{
                return(arr as Array<AnyObject>?,true,nil)
            }
            else{
                return(arr as Array<AnyObject>?,false,nil)
            }
            
            // success ...
        } catch let error as NSError {
            // failure
            print("Fetch failed: \(error.localizedDescription)")
            return(nil,true,error)
        }
        
        
    }
    
    //MARK: Test
    func getallData(Entity:String,Stream_ID:Int) -> (Array<AnyObject>?) {
        
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: Entity)
//        let predicate = NSPredicate(format: "\(predicate_str)=%@", parameter as! CVarArg)
        let predicate = NSPredicate(format: "ANY streams.id == \(Stream_ID)")
        
        fetchRequest.predicate=predicate
        
        do {
            let arr = try context.fetch(fetchRequest)
            if !arr.isEmpty{
                return(arr as Array<AnyObject>?)
            }
            else{
                return(arr as Array<AnyObject>?)
            }
            
            // success ...
        } catch let error as NSError {
            // failure
            print("Fetch failed: \(error.localizedDescription)")
            return(nil)
        }
        
        
    }

    //MARK: Current Stream 
    func currentStreamOfflineData(Entity:String,Stream_ID:Int,parameter:Any) -> (Array<AnyObject>?) {
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: Entity)
        let predicate = NSPredicate(format: "ANY offline_library.id == \(Stream_ID)", parameter as! CVarArg)
        fetchRequest.predicate=predicate
        
        do {
            
            let arr = try context.fetch(fetchRequest)
            if !arr.isEmpty{
                return(arr as Array<AnyObject>?)
            }
            else{
                return(arr as Array<AnyObject>?)
            }
            
            // success ...
        } catch let error as NSError {
            // failure
            print("Fetch failed: \(error.localizedDescription)")
            return(nil)
        }
        
        
    }
    
    //MARK:    Save Data   
    
    func save_data (arr_data:[AnyObject],completion:@escaping (Bool)-> Void) {
        
        //let groupA = DispatchGroup()
        
        for dict in arr_data {
            
           // groupA.enter()
            
            let stream = NSEntityDescription.insertNewObject(forEntityName: "Streams", into: context) as! Streams
            
            stream.id = (dict["id"] as? NSNumber)!
            stream.title = dict["title"] as? String
            stream.url = dict["url"] as? String
            stream.status = dict["status"] as? String
            stream.updated_at = dict["updated_at"] as? String
            
            save_offlineLib(dict_offline: dict["offline_library"] as! [String:AnyObject], completion: { (finished:Bool, offline_lib:Offline_Library) in
                
                stream.offline = offline_lib
                appDelegate.saveContext()
                
               // groupA.leave()
            })
            
        }
        
        //groupA.notify(queue: DispatchQueue.main) {
            completion(true)
        //}
     }
    
    //MARK: Offline Library
    
    func save_offlineLib (dict_offline:[String:AnyObject], completion:@escaping (Bool , _ offline_lib: Offline_Library)->Void){
        
        let offline_lib = NSEntityDescription.insertNewObject(forEntityName: "Offline_Library", into: context) as! Offline_Library
        
        offline_lib.id = (dict_offline["id"] as? NSNumber)!
        offline_lib.title = dict_offline["title"] as? String
        offline_lib.status = dict_offline["status"] as? String
        
        save_tracks(arr_tracks: dict_offline["tracks"] as! [[String : AnyObject]]) { (finished:Bool, tracks:[Tracks]) in
            
            offline_lib.tracks = NSSet(array: tracks)
            //appDelegate.saveContext()
            completion(true,offline_lib)
        }
    }
    
    //MARK: Tracks
    func save_tracks(arr_tracks:[[String:AnyObject]], completion:@escaping (Bool,_ tracks:[Tracks])->Void){
        
        var arr_tracksmodel = [Tracks]()
        
        // let groupC = DispatchGroup()
        
        for dict in arr_tracks {
            
          //  groupC.enter()
            
            let track = NSEntityDescription.insertNewObject(forEntityName: "Tracks", into: context) as! Tracks
            //set the entity values
            track.id = (dict["id"] as? NSNumber)!
            track.title = dict["title"] as? String
            track.path = ""
            track.updated_at = dict["updated_at"] as? String
            //appDelegate.saveContext()
            arr_tracksmodel.append(track)
            
           // groupC.leave()
        }
        
        //groupC.notify(queue: DispatchQueue.main) {
            completion(true,arr_tracksmodel)
       // }
        //return arr_tracksmodel
    }
    
    func save_stream (arr_data:[AnyObject],completion:@escaping (Bool)-> Void) {
        
        //let groupA = DispatchGroup()
        
        for dict in arr_data {
            
            // groupA.enter()
            
            let stream = NSEntityDescription.insertNewObject(forEntityName: "Streams", into: context) as! Streams
            
            stream.id = (dict["id"] as? NSNumber)!
            stream.title = dict["title"] as? String
            stream.url = dict["url"] as? String
            stream.status = dict["status"] as? String
            stream.updated_at = dict["updated_at"] as? String
        }
        
        //groupA.notify(queue: DispatchQueue.main) {
        completion(true)
        //}
    }
    
    //MARK:  DELETE ALL 
    func DeleteTracksNotInList(arr_tracks:[NSNumber]) {
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Tracks")
        let predicate = NSPredicate(format: "NOT (id IN %@)", arr_tracks)
        
        fetchRequest.predicate=predicate
        
        do {
            let arr = try context.fetch(fetchRequest)
            if(!arr.isEmpty) {
                for object in arr   {
                    context.delete(object as! NSManagedObject)
                }
            }
            // success ...
        } catch let error as NSError {
            // failure
            print("Fetch failed: \(error.localizedDescription)")
        }
    }
    
    func DeleteStreamData() {
        
        for entity in ["Streams"] {
            
            // Create Fetch Request
            let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entity)
            
            // Create Batch Delete Request
            let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            
            do {
                try context.execute(batchDeleteRequest)
            } catch {
                // Error Handling
            }
        }
    }
    
    func DeleteOfflineLibrary() {
        
        self.DeleteAllFiles()
        
        for entity in ["Tracks","Offline_Library"] {
            
            // Create Fetch Request
            let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entity)
            
            // Create Batch Delete Request
            let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            
            do {
                try context.execute(batchDeleteRequest)
            } catch {
                // Error Handling
            }
        }
    }
    
    func DeleteAllData() {
        
        self.DeleteAllFiles()
        
        for entity in ["Tracks","Streams","Offline_Library"] {
            
            // Create Fetch Request
             let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entity)
            
            // Create Batch Delete Request
            let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            
            do {
                try context.execute(batchDeleteRequest)
            } catch {
                // Error Handling
            }
        }
    }
    
    
    func DeleteAllFiles() {
        let fileManager = FileManager.default
        do {
            let filePaths = try fileManager.contentsOfDirectory(atPath: documentsUrl.path)
            for filePath in filePaths {
                let full_path = documentsUrl.appendingPathComponent(filePath)
                try fileManager.removeItem(atPath: full_path.path)
                //try fileManager.removeItem(atPath: documentsUrl.path + filePath)
            }
        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }
    
}
