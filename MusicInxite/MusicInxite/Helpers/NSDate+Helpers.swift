//
//  NSDate+Helpers.swift
//  MusicInxite
//
//  Created by Kishor Lodhia on 15/10/16.
//  Copyright © 2016 Kishor Lodhia. All rights reserved.
//

import UIKit

var dateFormatter = DateFormatter()
extension NSDate{
    
   class func convertUTCToLocalDate(dateStr:String) -> String {
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone!
        
        let UTC_Date = dateFormatter.date(from: dateStr)
        
        dateFormatter.timeZone = NSTimeZone.local
        
        return dateFormatter.string(from: UTC_Date!)
    }
    
    func toString(format: String = "yyyy-MM-dd HH:mm:ss") -> String{
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self as Date)
    }
    
    func toUTCString(format: String = "yyyy-MM-dd HH:mm:ss") -> String{
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone!
        return dateFormatter.string(from: self as Date)
    }
    
   class func toDate(dateString: String, format: String = "yyyy-MM-dd HH:mm:ss") -> NSDate{
    
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: dateString)! as NSDate
    }
    
    

}
