//
//  EnterPinVC.swift
//  MusicInxite
//
//  Created by Pranav Panchal on 27/02/17.
//  Copyright © 2017 Kishor Lodhia. All rights reserved.
//

import UIKit

protocol AddSongsProtocol {
    func addSongsRequest()
}

protocol RemoveSongsProtocol {
    func removeSongsRequest()
}

class EnterPinVC: BaseVC,UITextFieldDelegate {
    
    @IBOutlet var textField: UITextField!
    @IBOutlet var userName: UILabel!
    @IBOutlet var screenTitle: UILabel!
    var userDict: [String:AnyObject]! = nil
    var AddProtocol:AddSongsProtocol?
    var RemoveProtocol:RemoveSongsProtocol?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        userDict = UserDefaults.standard.value(forKey: "User_Info") as! [String:AnyObject]
        userName.text = String(format: "Hello %@", userDict["name"] as! String)
        textField.addTarget(self, action:#selector(textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)

        // Do any additional setup after loading the view.
        textField.layer.shadowColor = UIColor.black.cgColor
        self.addBlurEffect()
        
        if AddProtocol == nil    {
            screenTitle.text = "Remove Song"
        }
        else    {
            screenTitle.text = "Add Song"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.TrackScreen(title: "EnterPinScreen")
        
        // Show Keypad by default
        textField.becomeFirstResponder()
    }
    
    //MARK: - Add Blur Effect
    func addBlurEffect()    {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.bringSubview(toFront: view)
        view.insertSubview(blurEffectView, at: 0)
    }
    
    //MARK: - TextField Delegates
    func textFieldDidChange(textField: UITextField) {
        
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.layer.borderWidth = 0.0;

        if(textField.text?.characters.count == 4)  {
            
            if(textField.text?.isEmpty)! {
                print("is empty")
            }
            else    {
                // Compare Logic
                let dbPin = String(format: "%@",userDict["pin"]! as! CVarArg)
                
                if(textField.text == dbPin) {
                    
                    textField.resignFirstResponder()
                    
                    if AddProtocol == nil    {
                        RemoveProtocol?.removeSongsRequest()
                    }
                    else    {
                        AddProtocol?.addSongsRequest()
                    }
                    
                    self.dismiss(animated: true) {
                    }
                }
                else    {
                    textField.layer.borderColor = UIColor.red.cgColor
                    textField.layer.borderWidth = 3.0
                    textField.layer.cornerRadius = 6.0
                    let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
                    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
                    animation.duration = 0.6
                    animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
                    textField.layer.add(animation, forKey: "shake")
                    textField.text = ""
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back_pressed (sender:UIButton){
        textField.resignFirstResponder()
        self.dismiss(animated: true) {
            
        }
    }
}
