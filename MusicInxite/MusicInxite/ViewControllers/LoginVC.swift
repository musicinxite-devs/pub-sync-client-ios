//
//  LoginVC.swift
//  MusicInxite
//
//  Created by Kishor Lodhia on 17/09/16.
//  Copyright © 2016 Kishor Lodhia. All rights reserved.
//

import UIKit


import NVActivityIndicatorView


class LoginVC: BaseVC, UITextFieldDelegate,UIPopoverPresentationControllerDelegate,UIAdaptivePresentationControllerDelegate {

    @IBOutlet weak var txt_email:UITextField!
    @IBOutlet weak var txt_password:UITextField!
    
    @IBOutlet weak var txt_forgot_email:UITextField!
    @IBOutlet weak var view_forgot:UIView!
    @IBOutlet weak var Hud_View:UIView!
    
    @IBOutlet var bars: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.TrackScreen(title: "LoginScreen")
        
        txt_email.text = UserDefaults.standard.value(forKey: "User_Email") as! String?
        txt_password.text = UserDefaults.standard.value(forKey: "User_Password") as! String?
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

//        if (UserDefaults.standard.value(forKey: "IsLoggedIn") as? String == "YES"){
//            self.performSegue(withIdentifier: "PlayerViewController", sender: self)
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- Share Button
    @IBAction func shareButton (sender:UIButton)    {
        print("share button clicked")
        
        if let DeviceIdentifier = UIDevice.current.identifierForVendor?.uuidString {
            let objectsToShare = [DeviceIdentifier]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            //New Excluded Activities Code
            activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList,UIActivityType.saveToCameraRoll,UIActivityType.postToFlickr,UIActivityType.postToVimeo,UIActivityType.postToWeibo]
            activityVC.popoverPresentationController?.sourceView = sender
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func information_pressed(sender:UIButton?)   {
        self.performSegue(withIdentifier: "SharePropover", sender: self)
    }
    
    //MARK:- TextField Return
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK:- Email Validation
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    //MARK:- check validation
    
    func isvalidInput() -> (message:String,isvalid:Bool) {
        
        if let text = txt_email.text , text.isEmpty{
            return ("Email should not be blank",false)
        }
        if !self.isValidEmail(testStr: txt_email.text!){
            return ("Email is Invalid",false)
        }
        if let text = txt_password.text , text.isEmpty{
            return ("Password should not be blank",false)
        }
        else{
            return ("Validation success",true)
        }
    }

    
    //MARK:- Login
    
    @IBAction func login (sender:UIButton) {
        let valid = self.isvalidInput()
        if(!valid.isvalid){
            showAlert(message: valid.message)
            return
        }
        else{
            
            if appDelegate.is_reachable{
                show_hud()
                sleep(1)
                
                APIManager.sharedInstance.login(user_email: txt_email.text!, Password: txt_password.text!, completion: { (dict, error) in
                    
                    self.bars.stopAnimating()
                    self.Hud_View.isHidden = true
                    
                    
                    if error == nil{
                        print(dict)
                        if dict["status"] as? NSNumber == 200{
                            
                            /*
                             if let userDict =  UserDefaults.standard.value(forKey: "User_Info") as? [String:AnyObject]{
                             
                             if userDict["customer_id"] as! NSNumber != dict["customer"]?["customer_id"] as! NSNumber{
                             // Delete all
                             DBAction.sharedInstance.DeleteAllData()
                             }
                             }
                             */
                            UserDefaults.standard.set(self.txt_email.text, forKey: "User_Email")
                            UserDefaults.standard.set(self.txt_password.text, forKey: "User_Password")
                            UserDefaults.standard.set("YES", forKey: "IsLoggedIn")
                            // print(dict["customer"])
                            UserDefaults.standard.set(dict["customer"], forKey: "User_Info")
                            
                            let dt = NSDate().toUTCString()
                            APIManager.sharedInstance.postActivity(eventName: "Login", eventTime: dt, completion: { (dict, error) in
                                
                            })
                            
                            DBAction.sharedInstance.save_data(arr_data: dict["streams"] as! [AnyObject], completion: { (finished:Bool) in
                                //DispatchQueue.main.async {
                                self.performSegue(withIdentifier: "PlayerViewController", sender: self)
                                //}
                            })
                        }
                        else{
                            self.showAlert(message: dict["message"] as? String)
                        }
                        
                    }
                    else{
                        self.showAlert(message: error?.localizedDescription)
                    }
                })
            }
                
            else{
                
                self.showAlert(message: "Network connection is not avilable.")
                //self.performSegue(withIdentifier: "PlayerViewController", sender: self)
            }
            
        }
    }
    
    //MARK:- Show HUD
    func show_hud(){
        
        self.Hud_View.isHidden = false
        Hud_View.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        Hud_View.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.Hud_View.alpha = 1.0
            self.Hud_View.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.bars.startAnimating()
            
        })

    }
    
    //MARK:- Forgot Password
    
    @IBAction func forgot_password (sender:UIButton) {
        
        view_forgot.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        view_forgot.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view_forgot.alpha = 1.0
            self.view_forgot.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    // MARK:- Forgot password view
    
    @IBAction func cancel_forgot_passwrod (sender:UIButton) {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_forgot.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view_forgot.alpha = 0.0
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                }
        })
    }
    
    @IBAction func send_forgot_passwrod (sender:UIButton) {
        
        if (txt_forgot_email.text?.isEmpty)!{
            showAlert(message: "Email should not be blank")
        }
        else if !self.isValidEmail(testStr: txt_forgot_email.text!){
            showAlert(message: "Email is Invalid")
        }
        
        else if !appDelegate.is_reachable{
            showAlert(message: "Network connection is not avilable.")
        }
        
        else{
          
            show_hud()
            sleep(2)
            
            APIManager.sharedInstance.forgot_password(user_email: txt_forgot_email.text!,  completion: { (dict, error) in
                
                self.bars.stopAnimating()
                self.Hud_View.isHidden = true
                
                
                if error == nil{
                    //print(dict)
                    self.showAlert(message: dict["message"] as? String)
                }
                else{
                    self.showAlert(message: error?.localizedDescription)
                }
                
                UIView.animate(withDuration: 0.25, animations: {
                    self.view_forgot.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                    self.view_forgot.alpha = 0.0
                    }, completion:{(finished : Bool)  in
                        if (finished)
                        {
                        }
                })
            })
        }
    }
    
    //MARK:- musicinxite.com
    
    @IBAction func musicInxite (sender:UIButton) {
        UIApplication.shared.open(NSURL(string: "http://www.musicinxite.com/") as! URL, options: [:]) { (finished:Bool) in
        }
    }

    
    //MARK:- Show Alert
    func showAlert(title:String = "MusicInxite",message:String?){
        
        var alert :UIAlertController!
        
        if let msg = message , msg.isEmpty{
            alert = UIAlertController(title: title, message: "", preferredStyle: .alert)
        }
        else{
         alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        }
        let ok = UIAlertAction(title: "Ok", style: .default, handler: { (action:UIAlertAction) in
            
        })
        alert.addAction(ok)
        self.present(alert, animated: true, completion: {
            
        })
    }
    
    func adaptivePresentationStyle(for controller:UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "PlayerViewController"){
            
        }
        
        if segue.identifier == "SharePropover" {
            let shareVC = segue.destination as! SharePropoverVC
            shareVC.preferredContentSize = CGSize(width:200, height:90)
            
            let popoverController = shareVC.popoverPresentationController
            
            if popoverController != nil {
                popoverController?.delegate = self
                popoverController!.backgroundColor = UIColor.init(colorLiteralRed: 231.0/255.0, green: 231.0/255.0, blue: 231.0/255.0, alpha: 1.0)
            }
        }
    }
    

}
