//
//  PLayList_Cell.swift
//  MusicInxite
//
//  Created by Kishor Lodhia on 09/10/16.
//  Copyright © 2016 Kishor Lodhia. All rights reserved.
//

import UIKit

class PLayList_Cell: UITableViewCell {

    @IBOutlet weak var btn:UIButton!
    @IBOutlet weak var lbl_streamName:UILabel!
    @IBOutlet weak var img_play:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
