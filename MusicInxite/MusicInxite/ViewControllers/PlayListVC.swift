//
//  PlayListVC.swift
//  MusicInxite
//
//  Created by Kishor Lodhia on 09/10/16.
//  Copyright © 2016 Kishor Lodhia. All rights reserved.
//

import UIKit

import CoreData

protocol playListDelegate {
    
    func StreamChanged(stream: Streams?)
}

class PlayListVC: BaseVC,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tbl:UITableView!
    @IBOutlet weak var version: UILabel!

    
    var arr_AllStrems = [Streams]()
    
    var current_stream:Streams?
    
    var  playListDelegateObject :playListDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tbl.tableFooterView = UIView(frame: CGRect.zero)
        tbl.estimatedRowHeight = 50.0
        tbl.rowHeight = UITableViewAutomaticDimension
        
        load_AllStreams()
        
        let appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString")
        let bundleVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion")
        let versionDetails = String(format:"v %@ (%@)",appVersion as! CVarArg,bundleVersion as! CVarArg)
        version.text = versionDetails
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.TrackScreen(title: "PlayerListScreen")
        
        NotificationCenter.default.addObserver(self, selector: #selector(Alert_Subscription_Expried), name: NSNotification.Name(rawValue: "Subscription_Expired"), object: nil)
    }
    
    func Alert_Subscription_Expried(){
        
        let alert = UIAlertController(title: "MusicInxite", message: "Your Subscription Expired", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: { (action:UIAlertAction) in
            
            self.dismiss(animated: true, completion: {
                
            })
        })
        alert.addAction(ok)
        self.present(alert, animated: true, completion: {
            
        })

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Get all streams List
    func load_AllStreams() {
        
        let fetchrequest: NSFetchRequest<Streams> = Streams.fetchRequest()
        fetchrequest.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true, selector: #selector(NSString.localizedCaseInsensitiveCompare(_:)))]
        fetchrequest.includesSubentities = true

        do{
            let arr_streams = try appDelegate.persistentContainer.viewContext.fetch(fetchrequest)
            if !arr_streams.isEmpty{
                arr_AllStrems = arr_streams
                
                tbl.reloadData()
            }
        }
        catch {
            print("Error with request: \(error)")
        }

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_AllStrems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PLayList_Cell
        
        let stream = arr_AllStrems[indexPath.row]
        
        cell.lbl_streamName.text = stream.title
        
        if current_stream?.id == stream.id {
            cell.lbl_streamName.textColor = UIColor.white
            cell.img_play.tintColor = UIColor.white
        }
        else{
            cell.lbl_streamName.textColor = UIColor.black
            cell.img_play.tintColor = UIColor.black
        }
        
        cell.btn.tag = indexPath.row
        cell.btn.addTarget(self, action: #selector(selected_stream(sender:)), for: .touchUpInside)
        
        
        return cell
    }

    
    //MARK: select Stream
    
    func selected_stream(sender:UIButton) {
        
        let stream = arr_AllStrems[sender.tag]
        
        if current_stream?.id != stream.id{
            if let delegate = self.playListDelegateObject {
                delegate.StreamChanged(stream: stream)
            }
        }
        
        self.dismiss(animated: true) {
        }
    }
    
    //MARK: Back button
    
    @IBAction func back_pressed (sender:UIButton){
         self.dismiss(animated: true) { 
            
        }
    }
}
