//
//  PlayerVC.swift
//  MusicInxite
//
//  Created by Kishor Lodhia on 17/09/16.
//  Copyright © 2016 Kishor Lodhia. All rights reserved.
//

import UIKit
import MediaPlayer
import ReachabilitySwift
import NVActivityIndicatorView
import CoreData
import Alamofire
import SwiftMessages
import FreeStreamer

class PlayerVC: BaseVC,playListDelegate,UIPopoverPresentationControllerDelegate,UIAdaptivePresentationControllerDelegate, FSAudioControllerDelegate, StormysRadioKitDelegate{
    
    // MARK: - Initialize Objects
    
    //RadioKit
    var radioKit : RadioKit = RadioKit()
    
    var myPlayer : AVPlayer?
    var playerLayer:AVPlayerLayer?
    var myplayerItem :AVPlayerItem?
    var myQueuePlayer: AVQueuePlayer?
    var pollingTimer: Timer?
    
    var fsPlayer : FSAudioController? //new FreeStreamer object
    var fsStream : FSAudioStream? // new FreeStreamer Object
    
    @IBOutlet var volumeView: MPVolumeView!
    @IBOutlet var controlView: UIView!
    
    @IBOutlet var connecting: NVActivityIndicatorView!
    @IBOutlet var music_bars: NVActivityIndicatorView!
    @IBOutlet var download : NVActivityIndicatorView!
    var volume_slider: UISlider!
    @IBOutlet weak var btn_play: UIButton!
    @IBOutlet weak var logout: UIButton!
    @IBOutlet weak var img_online : UIImageView!
    @IBOutlet weak var HudView : UIView!
    @IBOutlet weak var song_name :UILabel!
    var fullRotation : CABasicAnimation?
    var springAnimation: CASpringAnimation?
    @IBOutlet weak var add_Track: UIButton!
    @IBOutlet weak var remove_Track: UIButton!
    @IBOutlet weak var versionText: UILabel!

    
    var  is_playing = false
    var  is_playing_offline = false
    
    var queueObserving = false
    var PlayerObserving = false
    var reachability = Reachability()!
    var Subscription_Status = ("",false)
    var current_stream : Streams?
    static let sharedInstance = PlayerVC()
    var OfflineTime = ""
    var isAVPlayerON : Bool = false
    var isFadingGoingOn : Bool = false
    var isNewTrackAdded : Bool = false
    var isMethodStarts : Bool = false
    var isStreamChanged : Bool = false
    var isInfoClicked : Bool = false

    var  isInternetReachable = false
    var isViewLoadedFirstTime = true
    var timerInternetCheck : Timer?
    
    let audioController = FSAudioController()
    
    
    // MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(playerInterruptionHandler(noti:)), name: NSNotification.Name.AVAudioSessionInterruption, object: nil)
        let appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString")
        let bundleVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion")
        let versionDetails = String(format:"v %@ (%@)",appVersion as! CVarArg,bundleVersion as! CVarArg)
        versionText.text = versionDetails
        
        UIView.setAnimationsEnabled(false)
        self.perform(#selector(self.setAnimationEnabled), with: nil, afterDelay: 0.6)
        self.setupVolumeSlider()

        self.fullRotation = CABasicAnimation.init(keyPath: "transform.rotation")
        self.fullRotation!.fromValue = 0.0
        self.fullRotation!.toValue = CGFloat(2 * M_PI)
        self.fullRotation!.fillMode = kCAFillModeForwards
        self.fullRotation?.isAdditive = true
        self.fullRotation!.isRemovedOnCompletion = false
        self.fullRotation!.duration = 1.5
        self.fullRotation!.repeatCount = MAXFLOAT
        
        self.springAnimation = CASpringAnimation(keyPath: "transform.rotation")
        self.springAnimation!.damping = 12.5
        self.springAnimation!.duration = 1.5
        self.springAnimation!.repeatCount = 1
        
//        print("Presented by:\(self.presentedViewController)")
//        print("presenting by:\(self.presentingViewController)")
        
        // flag to check pause while reachability
        UserDefaults.standard.set(false, forKey: "IsPause")
        
        //RadioKit Authentication
        self.radioKit.authenticateLibrary(withKey1: 0x65397023, andKey2: 0x2c62110e)
        //self.radioKit.authenticateLibrary(withKey1: 0x1234, andKey2: 0x1234)
        self.radioKit.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.TrackScreen(title: "PlayerScreen")
        
        Check_Subscription()

        print("Presented by:\(self.presentedViewController)")
        print("presenting by:\(self.presentingViewController)")

        NotificationCenter.default.addObserver(self, selector: #selector(Check_Subscription), name: NSNotification.Name(rawValue: "Check_Subscription"), object: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if reachability.isReachable{
            
            self.startPolling()
            if !is_playing{
                retryConnection()
            }
            else if music_bars.isHidden {
                connecting.startAnimating()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        pollingTimer?.invalidate()
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self,
                                                  name: ReachabilityChangedNotification,
                                                  object: reachability)
    }
    
    
    //MARK: - Information Screen Methods
    @IBAction func info_pressed(sender:UIButton?)   {
        if isInfoClicked {
            self.presentedViewController?.dismiss(animated: false, completion: nil)
        }
        else    {
            self.performSegue(withIdentifier: "PropverTrackButton", sender: self)
        }
    }
    
    func adaptivePresentationStyle(for controller:UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    //MARK: - Rechability Notification
    func reachabilityChanged(note: NSNotification) {
        
        let reachability = note.object as! Reachability
        
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
            if isViewLoadedFirstTime {
                isInternetReachable = true
                isViewLoadedFirstTime = false
                self.startPlayingOnline()
            }else{
                if isInternetReachable{
                    
                }else{
                    isInternetReachable = true
                    if let timerInternetCheck = timerInternetCheck {
                        timerInternetCheck.invalidate()
                        self.timerInternetCheck = nil
                    }
                    self.timerInternetCheck = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: {_ in
                        if  self.isInternetReachable {
                            self.startPlayingOnline()
                        }
                    })
//                    self.timerInternetCheck?.fire()

                }
            }
        } else {
            
            if isViewLoadedFirstTime{
                isInternetReachable = false
                isViewLoadedFirstTime = false
                self.startPlayingOffline()
            }else{
                if isInternetReachable{
                    isInternetReachable = false
                    
                    if let timerInternetCheck = timerInternetCheck {
                        timerInternetCheck.invalidate()
                        self.timerInternetCheck = nil
                    }
                    self.timerInternetCheck = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: {_ in
                        if !self.isInternetReachable  {
                            self.startPlayingOffline()
                        }
                    })
//                    self.timerInternetCheck?.fire()
                    
                }else{
                    
                }
            }
        }
    }
    
    func startPlayingOnline(){
        
            print("called once internet reachable")
            self.isMethodStarts = true
            self.add_Track.isEnabled = true
            self.remove_Track.isEnabled = true
            
            //Remove MyQueuePlayer
            
            // Sagar - Test to check offline play resume
            if self.queueObserving {
                self.myQueuePlayer?.removeObserver(self, forKeyPath: "currentItem")
                self.myQueuePlayer?.removeAllItems()
                self.myQueuePlayer = nil
            }
        
            self.playOnline()
            
    }

    func startPlayingOffline() {
        
            print("Network not reachable")
            print("Network not reachable method called once")
            isMethodStarts = true
            add_Track.isEnabled = false
            remove_Track.isEnabled = false
            
            //Remove FSPlayer
            self.fsPlayer?.pause()
            self.radioKit.stopStream()
        
            connecting.startAnimating()
            music_bars.stopAnimating()
            stopButtonRotateView()
            song_name.text! = ""
        
            self.playOffline()

    }
    
    //MARK: - Load data
    func setupCurrentStream(){
        
        let stream_id = UserDefaults.standard.integer(forKey: "current_Stream_ID")
        if stream_id == 0 {
            load_current_Stream()
        }
        else{
            let check = DBAction.sharedInstance.checkDuplication(Entity: "Streams", predicate_str: "id", ID: NSNumber(value: stream_id))
            
            if check.isExists {
                current_stream = check.arr?[0] as? Streams
            }
        }
    }
    
    
    func load_current_Stream()
    {
        let fetchrequest: NSFetchRequest<Streams> = Streams.fetchRequest()
        fetchrequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        
        do{
            let arr_streams = try appDelegate.persistentContainer.viewContext.fetch(fetchrequest)
            if !arr_streams.isEmpty{
                
                current_stream = arr_streams[0]
                
                UserDefaults.standard.set(current_stream?.id, forKey: "current_Stream_ID")
            }
        }
        catch {
            print("Error with request: \(error)")
        }
    }
    
    func load_remaining_download_song(){
    
        if current_stream == nil {
            return
        }
        
//        print(current_stream?.offline?.id as Any)
        let remaining_tracks = DBAction.sharedInstance.check_remaining_downloads(Entity: "Tracks", stream_id: current_stream?.offline?.id as! Int, predicate_str: "path", parameter: "")
        
        if remaining_tracks.isExists{
            
            DispatchQueue.main.sync {
                self.download.startAnimating()
            }
            
            let arr_tracks = remaining_tracks.arr as! [Tracks]
            //let arr_tracks = current_stream?.offline?.tracks?.allObjects as! [Tracks]
            
            for track in arr_tracks {
                
                //print(track)
                
                let destination: DownloadRequest.DownloadFileDestination = { _, response in
                    let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                    let fileURL = documentsURL.appendingPathComponent("\(track.id)" + "_" + response.suggestedFilename!)
                    return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
                }
                
                //print(destination)
                
                let urlString = BASE_URL + "/track/download/" + "\(track.id)"
                print("downloaded url",urlString)
                
                Alamofire.download(urlString, to: destination).response { response in
                    //print(response)
                    print("Finished" ,response.request?.url as Any)

                    if response.error == nil {
                        
                        let str = response.destinationURL?.lastPathComponent
                        track.path = str
                        appDelegate.saveContext()
                        
                        let isDownloadCompleted = DBAction.sharedInstance.check_remaining_downloads(Entity: "Tracks", stream_id: self.current_stream?.offline?.id as! Int, predicate_str: "path", parameter: "")
                        if !isDownloadCompleted.isExists   {
                            DispatchQueue.main.async {
                                self.download.stopAnimating()
                            }
                        }
                    }
                    else{
                        print("Error download:\(response.error.debugDescription)")
                        DispatchQueue.main.async {
                            self.download.stopAnimating()
                        }
                    }
                }
            }
        }
        else    {
            DispatchQueue.main.async {
                self.download.stopAnimating()
            }
        }
    }
    
    //MARK: - Local File
    
    func local_Downloaded_files() -> Bool {
        
        var hasDownloadedTracks = false
        
        if let stream = current_stream{
            
            if let arr_tracks = stream.offline?.tracks?.allObjects as? [Tracks] , !arr_tracks.isEmpty {
                
                var arr = [AVPlayerItem]()
                
                for track in arr_tracks {
                    
                    if track.path != "" {
                        
                        let filePath = documentsUrl.appendingPathComponent(track.path!)
                        let a1 = AVPlayerItem(url: filePath)
                       // a1.addObserver(self, forKeyPath: "timedMetadata", options: [.new , .old], context: nil)
                        arr.append(a1)
                        
                        if myQueuePlayer != nil {
                            
                            if self.myQueuePlayer?.items().count != 0  {
                                if (myQueuePlayer?.items().contains(a1))! {
                                    print("remove old tracks")
                                }
                                else    {
                                    print("new element added in tracklist")
                                    myQueuePlayer?.insert(a1, after: myQueuePlayer?.items().last)
                                }
                            }
                        }
                    }
                }
                
                self.removeOldTracks(arr)
                
                if myQueuePlayer == nil {
                    myQueuePlayer = AVQueuePlayer(items: arr)
                }
                
                if !arr.isEmpty{
                    hasDownloadedTracks = true
                }
            }
        }
        
        return hasDownloadedTracks
    }

    /*func local_Downloaded_files() -> Bool {
        
//        myQueuePlayer?.volume = volume_slider.value
        
        var hasDownloadedTracks = false
        
        if let stream = current_stream {
            
            if let arr_tracks = stream.offline?.tracks?.allObjects as? [Tracks] , !arr_tracks.isEmpty {
                
                //var arr = [AVPlayerItem]()
                var arr = [FSPlaylistItem]()
                
                for track in arr_tracks {
                    
                    if track.path != "" {
                        
                        let filePath = documentsUrl.appendingPathComponent(track.path!)
                        //let a1 = AVPlayerItem(url: filePath)
                        let item = FSPlaylistItem.init()
                        item.url = filePath
                        arr.append(item)
                        
                        if self.fsPlayer != nil {
                            if self.fsPlayer?.countOfItems() != 0 {
                                self.fsPlayer?.add(item)
                            }
                        }
                        
                        /*if myQueuePlayer != nil {
                            
                            if self.myQueuePlayer?.items().count != 0  {
                                if (myQueuePlayer?.items().contains(a1))! {
                                    print("remove old tracks")
                                }
                                else    {
                                    print("new element added in tracklist")
                                    myQueuePlayer?.insert(a1, after: myQueuePlayer?.items().last)
                                }
                            }
                        }*/
                    }
                }
                
                /*self.removeOldTracks(arr)

                if myQueuePlayer == nil {
                    myQueuePlayer = AVQueuePlayer(items: arr)
                }*/

                
                if !arr.isEmpty{
                    hasDownloadedTracks = true
                }
            }
        }
        
        return hasDownloadedTracks
    }*/
    
    func endQueue(noti:NSNotification)  {
        if let item = noti.object as? AVPlayerItem{
            item.seek(to: kCMTimeZero)
            myQueuePlayer?.insert(item, after: nil)
        }
    }
    
    func removeOldTracks(_ arr:[AVPlayerItem]) {
        if isStreamChanged  {
            if myQueuePlayer != nil {
                for items in (self.myQueuePlayer?.items())!    {
                    if (arr.contains(items)) {
                    }
                    else    {
                        print("remove old tracks")
                        self.myQueuePlayer?.remove(items)
                    }
                }
            }
        }
        isStreamChanged = false
    }
    
    //MARK: - Polling Methods
    func startPolling ()    {
        pollingTimer = Timer.scheduledTimer(timeInterval: 1800.0, target: self, selector: #selector(self.timerFired), userInfo: nil, repeats: true)
    }
    
    func timerFired(_ timer: Timer) {
        if self.reachability.isReachable{
            self.authenticate()
        }
    }
    
    func authenticate() {
        if Subscription_Status.1 {
            DispatchQueue.main.async {
                // Authenticate
                let useremail = UserDefaults.standard.string(forKey: "User_Email")
                let password = UserDefaults.standard.string(forKey: "User_Password")
                APIManager.sharedInstance.login(user_email: useremail!, Password: password!, completion: { (dict, error) in
                    if error == nil{
                        if dict["status"] as? NSNumber == 200  {
                            // Login Success
                            UserDefaults.standard.removeObject(forKey: "User_Info")
                            UserDefaults.standard.set(dict["customer"], forKey: "User_Info")
                            self.syncStreams(dict)
                        }
                        else  {
                            // Login Failed
                            self.logout_Pressed(sender: nil)
                        }
                    }
                })
            }
        }
    }
    
    func syncStreams(_ dict:[String:AnyObject])  {
        
        if reachability.isReachable{
            print("Sync Tracks")
            // Check Stream UpdateDateTime
            let arr_data = dict["streams"] as! [AnyObject]
            
            for (idx, _) in arr_data.enumerated()   {
                
                let dict = arr_data[idx] as! [String:AnyObject]
                let stream_id = dict["id"] as! NSNumber
                let streamUpdate = dict["updated_at"] as? String
                
                if (current_stream?.id == stream_id)    {
                    let responseDate = NSDate.toDate(dateString: streamUpdate!)
                    let DBStreamDate = NSDate.toDate(dateString: (current_stream?.updated_at)!)
                    
                    if responseDate.compare(DBStreamDate as Date) == .orderedDescending   {
                        
                        // Delete and Insert Stream Data
                        DBAction.sharedInstance.DeleteStreamData()
                        DBAction.sharedInstance.save_stream(arr_data: arr_data , completion: { (finished:Bool) in
                            appDelegate.saveContext()
                        })
                        
                        // Check Offline Library
                        let offlineLibrary = dict["offline_library"] as! [String: AnyObject]
                        let compare_id = DBAction.sharedInstance.getallData(Entity: "Offline_Library", Stream_ID: current_stream?.id as! Int) as! [Offline_Library]
                        var db_id = NSNumber()
                        for library in compare_id    {
                            db_id = library.id
                        }
                        
                        if(Int(db_id) == Int((offlineLibrary["id"] as? NSNumber)!))   {
                            // Library Updated
                            print("Library updated")
                            self.syncOfflineTracks(trackList: dict["offline_library"] as! [String : AnyObject])
                        }
                        else    {
                            // Library Changed
                            print("Library Completely Changed")
                            DBAction.sharedInstance.DeleteOfflineLibrary()
                            DBAction.sharedInstance.save_offlineLib(dict_offline: dict["offline_library"] as! [String : AnyObject], completion: { (finished:Bool, offline_lib:Offline_Library) in
                            })
                        }
                        
                        UserDefaults.standard.removeObject(forKey: "current_Stream_ID")
                        UserDefaults.standard.set(stream_id, forKey: "current_Stream_ID")
                        appDelegate.saveContext()
                        self.setupCurrentStream()
                        self.retryConnection()
                        if PlayerObserving{
                            removeKVOmyplayer()
                        }
                        self.playOnline()
                    }
                    else    {
                        self.syncOfflineTracks(trackList: dict["offline_library"] as! [String : AnyObject])
                        appDelegate.saveContext()
                        if isNewTrackAdded  {
                            self.setupCurrentStream()
                            self.retryConnection()
                            if PlayerObserving{
                                removeKVOmyplayer()
                            }
                            self.playOnline()
                            isNewTrackAdded = false
                        }
                    }
                }
            }
        }
    }
    
    func syncOfflineTracks(trackList:[String:AnyObject])    {
        if reachability.isReachable {
            let arr_Track = trackList["tracks"] as! [[String : AnyObject]]
            var arr_id = [NSNumber]()
            let arr_tracks = DBAction.sharedInstance.currentStreamOfflineData(Entity: "Tracks", Stream_ID: current_stream?.offline?.id as! Int, parameter: "") as! [Tracks]
            var offline_ids = [NSNumber]()
            
            for tracks in arr_tracks    {
                let db_id = tracks.id
                offline_ids.append(db_id)
            }
            
            for dict in arr_Track   {
                let off_ID = dict["id"] as? NSNumber
                arr_id.append(off_ID!)
                
                if offline_ids.contains(off_ID!) {
                    // Update Record
                    print(offline_ids.index(of: off_ID!)!)
//                        let indexvalue = offline_ids.index(of: off_ID!)!
//                        let single_track = arr_tracks?[indexvalue]
//                        single_track?.title = dict["title"] as! String?
//                        single_track?.updated_at = dict["updated_at"] as! String?
                }
                else    {
                    // New Track Added
                    isNewTrackAdded = true
                    DBAction.sharedInstance.save_tracks(arr_tracks: [dict], completion: { (finished:Bool, tracks:[Tracks]) in
                        print("new track inserted")
                        print(tracks)
                    })
                }
            }
            
            DBAction.sharedInstance.DeleteTracksNotInList(arr_tracks: arr_id)
        }
    }
    
    //MARK: - Try Reconnect Stream
    func retryConnection () {
        connecting.startAnimating()
        music_bars.stopAnimating()
        song_name.text = ""
        self.is_playing = false
        stopButtonRotateView()
    }
    
    //MARK: - Play File
    func playOnline()  {
        if Subscription_Status.1{
            
            self.is_playing_offline = false
            self.myQueuePlayer?.pause()
            
            DispatchQueue.global(qos: .utility).async {
                self.load_remaining_download_song()
            }
            
            DispatchQueue.main.async {
                self.playRemoteFile()
            }
        }
        
        if !OfflineTime.isEmpty{
            APIManager.sharedInstance.postActivity(eventName: "Offline", eventTime: OfflineTime, completion: { (dict, error) in
                self.OfflineTime = ""
            })
        }
    }
    
    func playRemoteFile() {
        
        self.img_online.image = UIImage(named: "online_play")
        
        if Thread.isMainThread {
            print("Main Thread playing")
        }
        
        //let fileUrl = "https://musicinxite.com:2199/tunein/dapaolo_dphq.pls"
        
        if current_stream == nil {
            return
        }
        
        if let myQueuePlayer = self.myQueuePlayer {
            self.is_playing_offline = false
            myQueuePlayer.pause()
        }
        
        //Play using radioKit
        //self.radioKit.setBufferWaitTime(15)
        //self.radioKit.setDataTimeout(10)
        self.radioKit.setStreamUrl(current_stream?.url, isFile: false)
        self.radioKit.startStream()
        self.updateStatusString()

        /*if let playeController = self.fsPlayer{
            if !UserDefaults.standard.bool(forKey: "IsPause") {
                playeController.play()
            }
            //self.fsPlayer?.activeStream.play()
        }else{
            
            let fileUrl = current_stream?.url
            let url = NSURL(string: fileUrl!)
            
            self.fsPlayer = FSAudioController.init(url: url as! URL)
            self.configBuffer()
            if !UserDefaults.standard.bool(forKey: "IsPause") {
                    self.fsPlayer?.play()
            }
            
            //get metadata
            self.fsPlayer?.onMetaDataAvailable = {(_ metaData: ([AnyHashable: Any]?)) -> Void in
                
                //Show title
                self.song_name.text = metaData?["StreamTitle"] as? String ?? ""
            }
            
            //get status for FSPlayer
            self.fsPlayer?.onStateChange = {(_ state: FSAudioStreamState) -> Void in
                
                switch (state) {
                    
                case .fsAudioStreamBuffering:
                    self.is_playing = false
                    self.connecting.startAnimating()
                    self.music_bars.stopAnimating()
                    self.stopButtonRotateView()
                    self.btn_play.isUserInteractionEnabled = false
                    
                case .fsAudioStreamPlaying:
                    self.is_playing = true
                    self.connecting.stopAnimating()
                    self.music_bars.startAnimating()
                    self.rotateView()
                    self.btn_play.isUserInteractionEnabled = true
                    
                case .fsAudioStreamPaused:
                    self.is_playing = false
                    self.music_bars.pauseAnimating()
                    self.stopButtonRotateView()
                    self.btn_play.isUserInteractionEnabled = true
                    
                default:
                    self.is_playing = false
                }
                self.HudView.isHidden = true
            }
     
//            self.addKVOmyPlayer()
        }*/
    
        
        //OLD CODE
    
//        let playerasset = AVURLAsset.init(url: url as! URL)
//        var item = AVPlayerItem(asset)

        /*myplayerItem = AVPlayerItem(url: url as! URL)
        if myPlayer == nil  {
            print("avplayer was null")
            myPlayer = AVPlayer(playerItem: myplayerItem)
        }
        else    {
            print("changing assests")
            myPlayer?.replaceCurrentItem(with: myplayerItem)
        }
        playerLayer?.player = myPlayer
        myPlayer?.volume = 0.0
        myPlayer?.isMuted = false
//        myPlayer?.automaticallyWaitsToMinimizeStalling = false
        myPlayer?.currentItem?.preferredForwardBufferDuration = 15.0
        addKVOmyPlayer()*/
        //myPlayer.playImmediately(atRate: 1)
    }

    //MARK:- Get RadioKitStatus
    func updateStatusString() {
        switch self.radioKit.getStreamStatus() {
        case SRK_STATUS_PLAYING:
            if self.radioKit.currTitle != nil {
                self.song_name.text = radioKit.currTitle
            }
            break
        default:
            self.song_name.text = ""
            break
        }
    }
    
    //MARK:- Radiokit Delegate methods
    func srkConnecting() {
        print("connecting..")
        /*self.is_playing = false
        self.connecting.startAnimating()
        self.music_bars.stopAnimating()
        self.stopButtonRotateView()*/
        self.btn_play.isUserInteractionEnabled = false
    }
    func srkisBuffering() {
        print("buffering..")
        self.is_playing = false
        self.connecting.startAnimating()
        self.music_bars.stopAnimating()
        self.stopButtonRotateView()
        self.btn_play.isUserInteractionEnabled = false
    }
    func srkPlayStarted() {
        print("playstarted..")
        self.is_playing = true
        self.connecting.stopAnimating()
        self.music_bars.startAnimating()
        self.rotateView()
        self.btn_play.isUserInteractionEnabled = true
    }
    func srkPlayStopped() {
        print("playstopped..")
        /*self.is_playing = false
        self.connecting.stopAnimating()
        self.music_bars.stopAnimating()
        self.stopButtonRotateView()
        self.btn_play.isUserInteractionEnabled = false*/
    }
    func srkPlayPaused() {
        print("playpaused..")
        self.is_playing = false
        self.music_bars.pauseAnimating()
        self.stopButtonRotateView()
        self.btn_play.isUserInteractionEnabled = true
    }
    func srkNoNetworkFound() {
        print("NoNetwork..")
        /*self.is_playing = false
        self.connecting.stopAnimating()
        self.music_bars.stopAnimating()
        self.stopButtonRotateView()
        self.btn_play.isUserInteractionEnabled = false*/
    }
    func srkurlNotFound() {
        print("Nourlfound..")
        /*self.is_playing = false
        self.connecting.stopAnimating()
        self.music_bars.stopAnimating()
        self.stopButtonRotateView()
        self.btn_play.isUserInteractionEnabled = false*/

    }
    func srkMetaChanged() {
        print("Metachanged..")
        if self.radioKit.currTitle != nil {
            self.song_name.text = radioKit.currTitle
        }
    }
    func srkRealtimeMetaChanged(_ title: String!, withUrl url: String!) {
        print("Realtimemetachanged..")
    }
    func srkAudioSuspended() {
        print("Audiosuspended..")
    }
    func srkAudioWillBeSuspended() {
        print("Audiowillsuspended..")
    }
    func srkAudioResumed() {
        print("AudioResumed..")
    }
    func srkFileComplete() {
        print("FileComplete..")
    }
    
    // MARK: - KVO Methods
    func playInstalled (noti:NSNotification){
        print("Playback stalled \(noti)")
    }
    
    func playfailed (noti:NSNotification){
        print("Playback Failed \(noti)")
        if reachability.isReachable{
            if PlayerObserving{
                removeKVOmyplayer()
            }
            playOnline()
        }
    }
    
    func playerInterruptionHandler (noti:NSNotification){
        if noti.name == NSNotification.Name.AVAudioSessionInterruption
            && noti.userInfo != nil {
            
            var info = noti.userInfo!
            var intValue: UInt = 0
            (info[AVAudioSessionInterruptionTypeKey] as! NSValue).getValue(&intValue)
            if let type = AVAudioSessionInterruptionType(rawValue: intValue) {
                switch type {
                case .began:
                    print("Interrupt Started")
                    self.interruptionStarted()
                case .ended:
                    print("Interrupt Ended")
                    self.interrptionEnded()
                }
            }
        }
    }
    
    func interruptionStarted()  {
        if (myPlayer?.currentItem?.timedMetadata != nil)    {
            isAVPlayerON = true
        }
        else if (myQueuePlayer?.currentItem?.asset.commonMetadata != nil)   {
            isAVPlayerON = false
        }
        
        myPlayer?.pause()
        myQueuePlayer?.pause()
        is_playing=false
        stopButtonRotateView()
        music_bars.pauseAnimating()
    }
    
    func interrptionEnded() {
        do  {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            do {
                try AVAudioSession.sharedInstance().setActive(true)
                print("AVAudioSession is Active")
            } catch let error as NSError {
                print(error.localizedDescription)
            }
            
        } catch {
            //TODO
        }
        if isAVPlayerON == true {
            myPlayer?.play()
            isAVPlayerON = false
        }
        else    {
            myQueuePlayer?.play()
        }
        is_playing = true
        if !UserDefaults.standard.bool(forKey: "IsPause") {
            rotateView()
            music_bars.startAnimating()
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if ((object! as AnyObject).isEqual(myPlayer?.currentItem) && keyPath == "status") {
            //if ((object! as AnyObject).isEqual(myplayerItem) && keyPath=="status"){
            print("Observer:\(myPlayer?.status)")
            
            if myPlayer?.status == AVPlayerStatus.readyToPlay{
                print("ReadyToPlay")
                //myPlayer.play();
                
                if self.queueObserving{
                    self.myQueuePlayer?.removeObserver(self, forKeyPath: "currentItem")
                    self.queueObserving = false
                }
                self.fadeOutOfflinePlayer()
            }
            else if myPlayer?.status == AVPlayerStatus.failed{
                print("Failed \(myPlayer?.currentItem?.error)")
            }
            else if myPlayer?.status == AVPlayerStatus.unknown {
                print("Unknown")
            }
        }
        // LikelyToKeepUp KVO
        else if ((object! as AnyObject).isEqual(myPlayer?.currentItem) && keyPath=="playbackLikelyToKeepUp")    {
            
            print("Likey Old value is",change?[.oldKey] as! NSNumber)
            print("Likey New value is",change?[.newKey] as! NSNumber)
            
            let oldValue = change?[.oldKey] as! NSNumber
            let newValue = change?[.newKey] as! NSNumber
            
            if(oldValue != newValue)    {
                if !isFadingGoingOn {
                    
                    print("\nLikelyToKeepUp KVO Called\n")
                    print("LTK state is: \(myPlayer?.timeControlStatus.rawValue)")
                    print("LTK like to keep up \(myPlayer?.currentItem?.isPlaybackLikelyToKeepUp)")
                    print("LTK buffer full: \(myPlayer?.currentItem?.isPlaybackBufferFull)")
                    print("LTK buffer empty: \(myPlayer?.currentItem?.isPlaybackBufferEmpty)")
                    print("LTK Player Rate: \(myPlayer?.rate)")
                    print("LTK QueuePlayer Rate: \(myQueuePlayer?.rate)")
                    
                    if reachability.isReachable {
                        print("Reachable via internet in KVO")
                        
                        if(newValue == 0)   {
                            print("\nLikelyToKeepFalse")
                            self.playOfflineWhenStalled()
                        }
                        
                        if(newValue == 1) {
                            print("\nLikelyTokeepTrue")
                            if PlayerObserving{
                                removeKVOmyplayer()
                            }
                            if self.queueObserving {
                                self.myQueuePlayer?.removeObserver(self, forKeyPath: "currentItem")
                                self.queueObserving = false
                            }
                            
                            self.myQueuePlayer?.pause()
                            self.playOnline()
                        }
                    }
                    else    {
                        print("Not Reachable via internet in KVO")
                    }
                }
            }
        }
        // Rate KVO
        else if ((object! as AnyObject).isEqual(myPlayer) && keyPath=="rate")   {
            
            let oldValue = change?[.oldKey] as! NSNumber
            let newValue = change?[.newKey] as! NSNumber
            
            if(oldValue != newValue)    {
                
                if !isFadingGoingOn {
                    
                    print("\nRate KVO Called\n")
                    print("Rate state is: \(myPlayer?.timeControlStatus.rawValue)")
                    print("Rate like to keep up \(myPlayer?.currentItem?.isPlaybackLikelyToKeepUp)")
                    print("Rate buffer full: \(myPlayer?.currentItem?.isPlaybackBufferFull)")
                    print("Rate buffer empty: \(myPlayer?.currentItem?.isPlaybackBufferEmpty)")
                    print("Rate Player Rate: \(myPlayer?.rate)")
                    print("Rate QueuePlayer Rate: \(myQueuePlayer?.rate)")
                    
                    if reachability.isReachable {
                        
                        if(myQueuePlayer?.rate == 0.0 && myPlayer?.rate == 0.0
                            && myPlayer?.currentItem?.isPlaybackLikelyToKeepUp == true
                            && myPlayer?.timeControlStatus.rawValue == 0
                            && isAVPlayerON == false) {
                            print("\nRate KVO Test")
                            self.playOfflineWhenStalled()
                            self.awakePlayerFromSleep()
                        }
                        
//                        if (myPlayer?.currentItem?.isPlaybackBufferFull == true)   {
//                            print("offline continues playing and we need to start online play manually")
//                            if PlayerObserving{
//                                removeKVOmyplayer()
//                            }
//                            if self.queueObserving{
//                                self.myQueuePlayer?.removeObserver(self, forKeyPath: "currentItem")
//                                self.queueObserving = false
//                            }
//                            
//                            self.myQueuePlayer?.pause()
//                            self.playOnline()
//                        }
                    }
                }
            }
        }
        // TimeMetaData KVO
        else if ((object! as AnyObject).isEqual(myPlayer?.currentItem) && keyPath=="timedMetadata") {
            //if ((object! as AnyObject).isEqual(myplayerItem) && keyPath=="status"){
            print("timemeta data KVO called")
            
            //if let meta = myPlayer.currentItem?.timedMetadata {
            self.setOnlinePlayerTitle()
        }
        // CurrentItem KVO
        else if ((object! as AnyObject).isEqual(myQueuePlayer) && keyPath=="currentItem")   {
            print("currentitem KVO called")
            if(myQueuePlayer?.rate == 1.0)   {
                self.setOfflinePlayerTitle(change: change)
            }
        }
        //TimeControlStatus KVO
        else if((object! as AnyObject).isEqual(myPlayer) && keyPath=="timeControlStatus")     {
            print("Old value is",change?[.oldKey] as! NSNumber)
            print("New value is",change?[.newKey] as! NSNumber)
            
            let oldValue = change?[.oldKey] as! NSNumber
            let newValue = change?[.newKey] as! NSNumber
            
            if(oldValue != newValue) {
                print("values not matched timecontrol satatus")
                if reachability.isReachable {
                    if(newValue == 0 && myPlayer?.currentItem?.isPlaybackLikelyToKeepUp == false) {
                        print("TimeControlStatus KVO Test")
                    }
                }
            }
        }
    }
    
    // MARK: - Set Title Methods
    func setOnlinePlayerTitle() {
        
        /*if (self.fsPlayer?.currentPlaylistItem) != nil {
            
            //print(meta.description)
            
            if let item = fsPlayer?.currentPlaylistItem   {
                
                print(item)
                //for item in arritems {
                    
                    /*if item.commonKey == "title"{
                        if let songname = item.value as? String{
                            print("song name is: \(songname)")
                            song_name.text = songname
                            MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyTitle:songname]
                            break
                        }
                    }*/
                //}
            }
            else    {
                song_name.text = "No Title"
                MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyTitle:song_name.text as Any]
            }
        }*/
        
        //OLD_CODE
        if (myPlayer?.currentItem?.timedMetadata) != nil {
            
            print("meta")
            
            //print(meta.description)
            
            if let arritems = myPlayer?.currentItem?.timedMetadata, !arritems.isEmpty   {
                
                for item in arritems {
                    
                    if item.commonKey == "title" {
                        if let songname = item.value as? String {
                            print("song name is: \(songname)")
                            song_name.text = songname
                            MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyTitle:songname]
                            break
                        }
                    }
                }
            }
            else    {
                song_name.text = "No Title"
                MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyTitle:song_name.text as Any]
            }
        }
    }
    
    func setOfflinePlayerTitle (change: [NSKeyValueChangeKey : Any]?)    {
        if let arritems = myQueuePlayer?.currentItem?.asset.availableMetadataFormats , !arritems.isEmpty{
            
            for item in arritems {
                
                let infoitems = myQueuePlayer?.currentItem?.asset.metadata(forFormat: item)
                
                for avitem in infoitems! {
                    if avitem.commonKey == "title" {
                        if let songname = avitem.value as? String{
                            print("song name is: \(songname)")
                            song_name.text = songname
                            MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyTitle:songname]
                        }
                        break
                    }
                }
            }
        }
        else    {
            if let itemRemoved = change?[.oldKey] as? AVPlayerItem {
                itemRemoved.seek(to: kCMTimeZero)
                myQueuePlayer?.insert(itemRemoved, after: nil)
            }
        }
    }
    
    // MARK: - Fading Implementation
    
    func fadeOutOfflinePlayer()    {

        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.fadeInOfflinePlayer), object: nil)

        if(self.myQueuePlayer != nil)   {
//             && self.myPlayer?.rate == 1.0
            if (self.myQueuePlayer?.volume)! > 0.1 {
                
                isFadingGoingOn = true
                self.myQueuePlayer?.volume = (self.myQueuePlayer?.volume)! - 0.1
                self.myPlayer?.volume = (self.myQueuePlayer?.volume)!

//                print("Fading Out Offline Player 01:",self.myQueuePlayer?.volume as Any)
//                print("Fading Out Online Player 01:",self.myPlayer?.volume as Any)
                
                if Thread.isMainThread {
                    self.perform(#selector(self.fadeOutOfflinePlayer), with: nil, afterDelay: 1.0)
                }
                else    {
                    self.perform(#selector(self.fadeOutOfflinePlayer), with: self.myQueuePlayer, afterDelay: 1.0, inModes: [RunLoopMode.commonModes])
                }
            }
            else {
                // Stop and get the sound ready for playing again
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.fadeOutOfflinePlayer), object: nil)
                myPlayer?.volume = 0.0
                self.startOnlinePlaying()
            }
        }
        else    {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.fadeOutOfflinePlayer), object: nil)
            self.startOnlinePlaying()
        }
    }
    
    func startOnlinePlaying() {
        isFadingGoingOn = false
        self.myQueuePlayer?.pause()
        self.fadeInOnlinePlayer()
        myPlayer?.play()
        self.img_online.image = UIImage(named: "online_play")
        add_Track.isEnabled = true
        remove_Track.isEnabled = true
        connecting.stopAnimating()
        music_bars.startAnimating()
        HudView.isHidden = true
        if !is_playing {
            is_playing = !is_playing
            rotateView()
        }
    }
    
    func fadeInOnlinePlayer()  {
        if myPlayer != nil  {
            if (self.myPlayer?.volume)! < volume_slider.value {
                isFadingGoingOn = true
                self.myPlayer?.volume = (self.myPlayer?.volume)! + 0.1
                
//                print("Fading In Online Player 02:",self.myPlayer?.volume as Any)
                
                if Thread.isMainThread {
                    self.perform(#selector(self.fadeInOnlinePlayer), with: nil, afterDelay: 1.0)
                }
                else    {
                    self.perform(#selector(self.fadeInOnlinePlayer), with: self.myPlayer, afterDelay: 1.0, inModes: [RunLoopMode.commonModes])
                }
            }
            else    {
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.fadeInOnlinePlayer), object: nil)
                isFadingGoingOn = false
                if myPlayer == nil  {
                    print("myplayer is nill")
                }
            }
        }
    }
    
    func fadeOutOnlinePlayer()  {
        if myPlayer != nil  {
            if (self.myPlayer?.volume)! > 0.1 {
                isFadingGoingOn = true
                self.myPlayer?.volume = (self.myPlayer?.volume)! - 0.1
                
//                print("Fading Out Online Player 03:",self.myPlayer?.volume as Any)
                
                if Thread.isMainThread {
                    self.perform(#selector(self.fadeOutOnlinePlayer), with: nil, afterDelay: 1.0)
                }
                else    {
                    self.perform(#selector(self.fadeOutOnlinePlayer), with: self.myPlayer, afterDelay: 1.0, inModes: [RunLoopMode.commonModes])
                }
            }
            else    {
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.fadeOutOnlinePlayer), object: nil)
                isFadingGoingOn = false
            }
        }
    }
    
    func fadeInOfflinePlayer() {
        
        if self.myQueuePlayer != nil    {
//             && self.myPlayer?.rate == 0
            if (self.myQueuePlayer?.volume)! < volume_slider.value  {
                
                isFadingGoingOn = true
                self.myQueuePlayer?.volume = (self.myQueuePlayer?.volume)! + 0.1
                
//                print("Fading In Offline Player: 04",self.myQueuePlayer?.volume as Any)
//                print("Fading In Online Player 04:",self.myPlayer?.volume as Any)

                if Thread.isMainThread{
                    self.perform(#selector(self.fadeInOfflinePlayer), with: nil, afterDelay: 1.0)
                }
                else    {
                    self.perform(#selector(self.fadeInOfflinePlayer), with: self.myQueuePlayer, afterDelay: 1.0, inModes: [RunLoopMode.commonModes])
                }
            }
            else {
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.fadeInOfflinePlayer), object: nil)
                isFadingGoingOn = false
            }
        }
    }
    
    func setAnimationEnabled()  {
        UIView.setAnimationsEnabled(true)
    }
    
    func awakePlayerFromSleep() {
        if PlayerObserving{
            removeKVOmyplayer()
        }
        
        if Subscription_Status.1 {
            
            DispatchQueue.global(qos: .utility).async {
                self.load_remaining_download_song()
            }
            
            DispatchQueue.main.async {
                self.playRemoteFile()
            }
        }
    }
    
    //MARK: - Switch TO OFFLINE
    func playOffline() {
        OfflineTime = NSDate().toUTCString()
        
        if Subscription_Status.1{
            DispatchQueue.main.async {
                
                self.fsPlayer?.pause()
                self.radioKit.stopStream()
                self.is_playing_offline = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    self.switchToOfflinePlay()
                    self.connecting.stopAnimating()
                    self.img_online.image = UIImage(named: "offline_play")
                }

            }
        }
    }
    
    func playOfflineWhenStalled()  {
        
        OfflineTime = NSDate().toUTCString()
        
        if Subscription_Status.1{
            
            DispatchQueue.main.async {
                if self.queueObserving{
                    self.myQueuePlayer?.removeObserver(self, forKeyPath: "currentItem")
                    self.queueObserving = false
                }
                
                self.myPlayer?.pause()
                self.swithOfflineWhenPlayerStalled()
                self.connecting.stopAnimating()
                self.add_Track.isEnabled = false
                self.remove_Track.isEnabled = false
                self.img_online.image = UIImage(named: "stall_wait")
                
                if !self.OfflineTime.isEmpty {
                    APIManager.sharedInstance.postActivity(eventName: "BufferLow", eventTime: self.OfflineTime, completion: { (dict, error) in
                        self.OfflineTime = ""
                    })
                }
            }
            
            if(self.myQueuePlayer?.items() == nil && self.download.animating) {
                self.showWarningAlert()
            }
        }
    }
    
    func swithOfflineWhenPlayerStalled()   {
        
        let hastracks =  local_Downloaded_files()
        
        if hastracks{
            
            self.myQueuePlayer?.addObserver(self, forKeyPath: "currentItem", options: NSKeyValueObservingOptions.old, context: nil)
            
            if let arritems = myQueuePlayer?.currentItem?.asset.availableMetadataFormats , !arritems.isEmpty{
                
                for item in arritems {
                    
                    let infoitems = myQueuePlayer?.currentItem?.asset.metadata(forFormat: item)
                    
                    for avitem in infoitems! {
                        if avitem.commonKey == "title"{
                            if let songname = avitem.value as? String{
                                print("song name is: \(songname)")
                                song_name.text = songname
                                MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyTitle:songname]
                            }
                            break
                        }
                    }
                }
            }
            
            queueObserving = true
            self.myQueuePlayer?.volume = 0.0
            self.fadeInOfflinePlayer()
            self.myQueuePlayer?.play()
            
            if !is_playing{
                is_playing = !is_playing
                rotateView()
                
                music_bars.startAnimating()
            }
        }
        else{
            retryConnection()
        }
    }
    
    func switchToOfflinePlay(){
        
//        if let myQueuePlayer = self.myQueuePlayer{
//            self.is_playing_offline = true
//            self.is_playing = true
//            myQueuePlayer.play()
//        }else{
        
            let hastracks =  local_Downloaded_files()
            
            if PlayerObserving{
                removeKVOmyplayer()
            }
            
            if hastracks {
                self.myQueuePlayer?.addObserver(self, forKeyPath: "currentItem", options: NSKeyValueObservingOptions.old, context: nil)
                
                if let arritems = myQueuePlayer?.currentItem?.asset.commonMetadata , !arritems.isEmpty{
                    
                    for item in arritems {
                        
                        if item.commonKey == "title"{
                            if let songname = item.value as? String{
                                print("song name is: \(songname)")
                                song_name.text = songname
                                MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyTitle:songname]
                            }
                            break
                        }
                    }
                }
                else {
                    song_name.text = "No Title"
                    MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyTitle:song_name.text as Any]
                }
                
                queueObserving = true
                //self.myQueuePlayer?.volume = 1.0
                self.fadeInOfflinePlayer()
                self.myQueuePlayer?.play()
                self.is_playing_offline = true
                self.is_playing = true
                
                //if !is_playing{
                    //is_playing = !is_playing
                    rotateView()
                    connecting.stopAnimating()
                    music_bars.startAnimating()
                    self.btn_play.isUserInteractionEnabled = true
                //}
            }
            else{
                retryConnection()
                
            }
       // }
    }
    
    //MARK: - Rotate Button
    func rotateView() {
    
        UIView.animate(withDuration: 1.5, delay: 0, options: [.curveLinear,.allowUserInteraction], animations: { () -> Void in
            self.btn_play.layer.removeAnimation(forKey: "spring360")
            self.btn_play.transform = self.btn_play.transform.rotated(by: CGFloat(M_PI_2))
            self.btn_play!.layer.add(self.fullRotation!, forKey: "360")
        })
    }
    
    func stopButtonRotateView() {
    
        self.btn_play.layer.add(self.springAnimation!, forKey: "spring360")
        
        UIView.animate(withDuration: 1.5, delay: 1.0, options: [.curveEaseOut], animations: { () -> Void in
            self.btn_play.layer.removeAnimation(forKey: "360")
        })
    }
    
    // MARK: - Play - Pause button
    @IBAction func play_pause (sender:UIButton){

        if self.is_playing_offline{
            
            if self.myQueuePlayer?.currentItem != nil {
                
                if self.is_playing {
                    self.myQueuePlayer?.pause()
                    music_bars.pauseAnimating()
                    stopButtonRotateView()
                    self.is_playing = false
                    
                }else{
                    self.myQueuePlayer?.play()
                    music_bars.startAnimating()
                    rotateView()
                    self.is_playing = true
                }
            }
        }else{
            
            //RadioKit Play Pause
            
            let currStatus = self.radioKit.getStreamStatus()
            if currStatus == SRK_STATUS_STOPPED || currStatus == SRK_STATUS_PAUSED {
                self.radioKit.startStream()
                music_bars.startAnimating()
                rotateView()
                self.is_playing = true

            } else if currStatus == SRK_STATUS_PLAYING {
                self.radioKit.pauseStream()
                music_bars.pauseAnimating()
                stopButtonRotateView()
                self.is_playing = false
            } else {
                self.radioKit.stopStream()
                music_bars.pauseAnimating()
                stopButtonRotateView()
                self.is_playing = false
            }
        }
        
        /*if self.is_playing_offline{
            
            if self.myQueuePlayer?.currentItem != nil {
            
                if self.is_playing{
                    self.myQueuePlayer?.pause()
                    music_bars.pauseAnimating()
                    stopButtonRotateView()
                    self.is_playing = false

                }else{
                    self.myQueuePlayer?.play()
                    music_bars.startAnimating()
                    rotateView()
                    self.is_playing = true
                }
            }
        }else{
            if is_playing {
                self.fsPlayer?.pause()
                music_bars.pauseAnimating()
                stopButtonRotateView()
                self.is_playing = false
                UserDefaults.standard.set(true, forKey: "IsPause")
            } else {
                
                self.configBuffer()
                self.fsPlayer?.pause()
                self.fsPlayer?.activeStream.play() //To restart stream
                music_bars.startAnimating()
                rotateView()
                self.is_playing = true
                UserDefaults.standard.set(false, forKey: "IsPause")
            }
        }*/
        
        /*if !music_bars.isHidden {
            
            is_playing = !is_playing
            if !is_playing {
                self.fsPlayer?.pause()
                self.myQueuePlayer?.pause()
                music_bars.pauseAnimating()
                stopButtonRotateView()
                
            } else {
                
                self.configBuffer()
                self.fsPlayer?.play()
                self.myQueuePlayer?.play()
                music_bars.startAnimating()
                rotateView()
                
            }
        }*/
        
        /*if !music_bars.isHidden{
            
            is_playing = !is_playing
            if !is_playing {
                
                if myPlayer?.rate == 1.0    {
                    isAVPlayerON = true
                }
                else if myQueuePlayer?.rate == 1.0   {
                    isAVPlayerON = false
                }
                
                self.fsPlayer?.pause()
                myQueuePlayer?.pause()
            }
            else{
                if isAVPlayerON == true {
                    self.fsPlayer?.play()
                    isAVPlayerON = false
                }
                else    {
                    self.fsPlayer?.play()
                }
                self.fsPlayer?.play()
            }
            
            //is_playing = !is_playing
            
            if !is_playing{
                music_bars.pauseAnimating()
                stopButtonRotateView()
            }
            else{
                music_bars.startAnimating()
                rotateView()
            }
        }*/
    }
    
    
    func configBuffer() {
        
        self.fsPlayer?.preloadNextPlaylistItemAutomatically = true
        self.fsPlayer?.enableDebugOutput = false
        self.fsPlayer?.automaticAudioSessionHandlingEnabled = true
        self.fsPlayer?.configuration = FSStreamConfiguration.init()
        self.fsPlayer?.configuration.automaticAudioSessionHandlingEnabled = true
        self.fsPlayer?.configuration.cacheEnabled = true
        
//        self.fsPlayer?.configuration.bufferSize = 50000
        
        //self.fsPlayer?.configuration.usePrebufferSizeCalculationInSeconds = true
        //self.fsPlayer?.configuration.usePrebufferSizeCalculationInPackets = true

        //self.fsPlayer?.configuration.requiredInitialPrebufferedByteCountForContinuousStream = 0
        //self.fsPlayer?.configuration.requiredInitialPrebufferedByteCountForNonContinuousStream = 0
    }
    
    //MARK: - Volume Slider
    func setupVolumeSlider() {
        for view in volumeView.subviews {
            if let vs = view as? UISlider {
                vs.minimumTrackTintColor = UIColor.init(red: 86.0/255.0, green: 208.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                vs.maximumTrackTintColor = UIColor.white
                vs.minimumValueImage = UIImage(named: "volume")
                vs.maximumValueImage = UIImage(named: "full_volume")
                volume_slider = vs
                break
            }
        }
        
        volumeView.showsRouteButton = false
        volumeView.setVolumeThumbImage(UIImage(named: "slider_dot"), for: .normal)
    }
    
    @IBAction func volume_changes (sender:UISlider) {
        self.fsPlayer?.volume = sender.value
        //myPlayer?.volume = sender.value
        myQueuePlayer?.volume = sender.value
    }
    
    //MARK: - play list
    @IBAction func moveToPlayList(sender:UIButton){
        
        self.performSegue(withIdentifier: "MoveToPLayList", sender: self)
    }
    
    // MARK: - Add/Remove Songs Request Methods
    @IBAction func addSongs_Pressed(sender:UIButton)   {
        self.performSegue(withIdentifier: "AddTrackEnterPin", sender: self)
    }
    
    @IBAction func removeSongs_Pressed(sender:UIButton)    {
        self.performSegue(withIdentifier: "RemoveTrackEnterPin", sender: self)
    }
    
    func trackRequest(type:String)  {
        DispatchQueue.main.async {
            let stream_id = UserDefaults.standard.integer(forKey: "current_Stream_ID")

                self.fsPlayer?.onMetaDataAvailable = {(_ metaData: ([AnyHashable: Any]?)) -> Void in
                    
                    //Show title
                    let songname = metaData?["StreamTitle"] as? String ?? ""
                    
                    if type != "" {
                        
                    APIManager.sharedInstance.trackRequest(stream_id: String(stream_id), meta_data: songname, type: type, completion: { (dict, error) in
                        print(dict)
                        if(error == nil)  {
                            self.showSuccessAlert()
                        }
                        else    {
                            self.showErrorAlert()
                        }
                    })
                }
            }
            
            //OLD Code
            /*if let arritems = self.myPlayer?.currentItem?.timedMetadata, !arritems.isEmpty   {
                for item in arritems {
                    if item.commonKey == "title"{
                        if let songname = item.value as? String{
                            print("song name is: \(songname)")
                            
                            APIManager.sharedInstance.trackRequest(stream_id: String(stream_id), meta_data: songname, type: type, completion: { (dict, error) in
                                print(dict)
                                if(error == nil)  {
                                    self.showSuccessAlert()
                                }
                                else    {
                                    self.showErrorAlert()
                                }
                            })
                            break
                        }
                    }
                }
            }*/
        }
    }
    
    func showSuccessAlert() {
        let view = MessageView.viewFromNib(layout: .CardView)
        view.button?.isHidden = true
        view.configureTheme(.success)
        view.configureContent(title: "Success", body: "Request Accepted")
        view.configureDropShadow()
        SwiftMessages.show(view: view)
    }
    
    func showErrorAlert()   {
        let view = MessageView.viewFromNib(layout: .CardView)
        view.button?.isHidden = true
        view.configureTheme(.error)
        view.configureContent(title: "Error", body: "Slow Connection")
        view.configureDropShadow()
        SwiftMessages.show(view: view)    
    }
    
    func showWarningAlert() {
        let view = MessageView.viewFromNib(layout: .CardView)
        view.button?.isHidden = true
        view.configureTheme(.warning)
        view.configureContent(title: "Warning", body: "Internet is too slow. Try again when you have fast internet")
        view.configureDropShadow()
        SwiftMessages.show(view: view)
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "MoveToPLayList"){
            
            let playlist = segue.destination as! PlayListVC
            playlist.playListDelegateObject = self
            playlist.current_stream = self.current_stream
        }
        
        if segue.identifier == "PropverTrackButton" {
            let aboutController = segue.destination as! PropoverAlertVC
            aboutController.preferredContentSize = CGSize(width:200, height:90)

            let popoverController = aboutController.popoverPresentationController
            
            if popoverController != nil {
                popoverController!.delegate = self
                aboutController.delegate = self
                popoverController!.backgroundColor = UIColor.init(colorLiteralRed: 231.0/255.0, green: 231.0/255.0, blue: 231.0/255.0, alpha: 1.0)
            }  
        }
        
        if segue.identifier == "NetworkPropover" {
            let aboutController = segue.destination as! NetworkPropoverVC
            aboutController.preferredContentSize = CGSize(width:210, height:170)
            
            let popoverController = aboutController.popoverPresentationController
            
            if popoverController != nil {
                aboutController.delegate = self
                popoverController!.delegate = self
                popoverController!.backgroundColor = UIColor.init(colorLiteralRed: 231.0/255.0, green: 231.0/255.0, blue: 231.0/255.0, alpha: 1.0)
            }
        }
        
        if segue.identifier == "LogoutPropover" {
            let aboutController = segue.destination as! LogoutPropoverVC
            aboutController.preferredContentSize = CGSize(width:145, height:90)
            
            let popoverController = aboutController.popoverPresentationController
            
            if popoverController != nil {
                popoverController!.delegate = self
                popoverController!.backgroundColor = UIColor.init(colorLiteralRed: 231.0/255.0, green: 231.0/255.0, blue: 231.0/255.0, alpha: 1.0)
            }
        }
        
        if (segue.identifier == "AddTrackEnterPin") {
            
            let enterPinVC = segue.destination as! EnterPinVC
            enterPinVC.AddProtocol = self
        }
        
        if (segue.identifier == "RemoveTrackEnterPin")  {
            
            let enterPinVC = segue.destination as! EnterPinVC
            enterPinVC.RemoveProtocol = self
        }
        
        if segue.identifier == "AddSongsAlert" {
            let ABAlertVC = segue.destination as! AddButtonVC
            ABAlertVC.preferredContentSize = CGSize(width:230, height:90)
            let popoverController = ABAlertVC.popoverPresentationController
            
            if popoverController != nil {
                ABAlertVC.delegate = self
                popoverController!.delegate = self
                popoverController!.backgroundColor = UIColor.init(colorLiteralRed: 231.0/255.0, green: 231.0/255.0, blue: 231.0/255.0, alpha: 1.0)
            }
        }
        
        if segue.identifier == "RemoveSongsAlert" {
            let RBAlertVC = segue.destination as! RemoveButtonVC
            RBAlertVC.preferredContentSize = CGSize(width:250, height:90)
            let popoverController = RBAlertVC.popoverPresentationController
            
            if popoverController != nil {
                RBAlertVC.delegate = self
                popoverController!.delegate = self
                popoverController!.backgroundColor = UIColor.init(colorLiteralRed: 231.0/255.0, green: 231.0/255.0, blue: 231.0/255.0, alpha: 1.0)
            }
        }
    }
    
    //MARK: - PlayList Delegate
    func StreamChanged(stream: Streams?) {
        
        if Subscription_Status.1 {
            
            if stream != nil {
                print(stream?.title as Any)
                print(stream?.url as Any)
                isStreamChanged = true
                current_stream = stream!
                UserDefaults.standard.set(current_stream?.id, forKey: "current_Stream_ID")
                
                if PlayerObserving{
                    removeKVOmyplayer()
                }
                
                if self.queueObserving{
                    self.myQueuePlayer?.removeObserver(self, forKeyPath: "currentItem")
                    self.queueObserving = false
                    self.myQueuePlayer?.removeAllItems()
                    self.myQueuePlayer = nil
                }
                
                if reachability.isReachable{
                    
                    self.retryConnection()
                    self.fsPlayer?.stop()
                    //Jigar
                    UserDefaults.standard.set(false, forKey: "IsPause")
                    //self.fsPlayer?.activeStream.delegate = nil
                    self.fsPlayer = nil
                    
                    DispatchQueue.global(qos: .utility).async {
                        self.load_remaining_download_song()
                    }
                    
                    self.playRemoteFile()
                }
                else{
                    UserDefaults.standard.set(false, forKey: "IsPause")
                    switchToOfflinePlay()
                }
            }
        }
    }
    
    
    //MARK: - Remote Control Events
    override func remoteControlReceived(with event: UIEvent?) {
        
        if event!.type == UIEventType.remoteControl {
            
            if event!.subtype == UIEventSubtype.remoteControlPlay {
                print("received remote play")
                if isAVPlayerON == true {
                    myPlayer?.play()
                    isAVPlayerON = false
                }
                else    {
                    myQueuePlayer?.play()
                }
                is_playing=true
                rotateView()
                music_bars.startAnimating()
                
            } else if event!.subtype == UIEventSubtype.remoteControlPause {
                print("received remote pause")
                if myPlayer?.rate == 1.0    {
                    isAVPlayerON = true
                }
                else if myQueuePlayer?.rate == 1.0   {
                    isAVPlayerON = false
                }
                
                myPlayer?.pause()
                myQueuePlayer?.pause()
                is_playing=false
                stopButtonRotateView()
                music_bars.pauseAnimating()
                
            } else if event!.subtype == UIEventSubtype.remoteControlNextTrack{
                print("received next")
                
            }else if event!.subtype == UIEventSubtype.remoteControlPreviousTrack{
                print("received previus")
                
            }else if event!.subtype == UIEventSubtype.remoteControlTogglePlayPause{
                print("received toggle")
            }
        }
    }
    
    //MARK: - Check Subscription
    func CalculateSubscription() -> (message : String, isValid : Bool) {
        
        let dict = UserDefaults.standard.value(forKey: "User_Info") as! [String:AnyObject]
        
        let StartStr = dict["subscription_start_date"] as! String
        let EndStr = dict["subscription_end_date"] as! String
        
        let start_Date = NSDate.toDate(dateString: StartStr)
        let end_Date = NSDate.toDate(dateString: EndStr)
        

        if start_Date.compare(NSDate() as Date) != .orderedAscending   {
            return (message: "Subscription is still not started", isValid: false)
        }
        else if (end_Date.compare(NSDate() as Date) != .orderedDescending){
            return (message:  "Subscription is expired",isValid: false)
        }
        else{
            return (message: "Subsciption is valid",isValid: true)
        }
    }
    
    func Check_Subscription(){
        Subscription_Status = CalculateSubscription()
        
        if !Subscription_Status.1{
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Subscription_Expired"), object: nil)
            
            myPlayer?.pause()
            myQueuePlayer?.pause()
            
            reachability.stopNotifier()
            NotificationCenter.default.removeObserver(self,
                                                      name: ReachabilityChangedNotification,
                                                      object: reachability)
            
            let alert = UIAlertController(title: "MusicInxite", message: Subscription_Status.0, preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default, handler: { (action:UIAlertAction) in
                self.logout_Pressed(sender: nil)
            })
            alert.addAction(ok)
            self.present(alert, animated: true, completion: {
                
            })
        }
        else{
            
            setupCurrentStream()
            
            NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)),name: ReachabilityChangedNotification,object: reachability)
            
            do{
                try reachability.startNotifier()
            }catch{
                print("could not start reachability notifier")
            }
        }
    }
    
    //MARK: - KVO for Myplayer
    func removeKVOmyplayer (){
        // Sagar
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: self.fsPlayer?.currentPlaylistItem)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemPlaybackStalled, object: self.fsPlayer?.currentPlaylistItem)
        PlayerObserving = false
        
        //OLD CODE
        /*NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: myPlayer?.currentItem)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemPlaybackStalled, object: myPlayer?.currentItem)
        self.myPlayer?.currentItem?.removeObserver(self, forKeyPath: "status")
        self.myPlayer?.currentItem?.removeObserver(self, forKeyPath: "playbackLikelyToKeepUp")
        self.myPlayer?.currentItem?.removeObserver(self, forKeyPath: "timedMetadata")
        self.myPlayer?.removeObserver(self, forKeyPath: "rate")
        self.myPlayer?.removeObserver(self, forKeyPath: "timeControlStatus")
        PlayerObserving = false*/
    }
    
    func addKVOmyPlayer(){
        // Sagar
        NotificationCenter.default.addObserver(self, selector: #selector(playInstalled(noti:)), name: NSNotification.Name.AVPlayerItemPlaybackStalled, object: self.fsPlayer?.currentPlaylistItem)
        NotificationCenter.default.addObserver(self, selector: #selector(playfailed(noti:)), name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: self.fsPlayer?.currentPlaylistItem)
        
        PlayerObserving = true
        
        //OLD CODE
        /*NotificationCenter.default.addObserver(self, selector: #selector(playInstalled(noti:)), name: NSNotification.Name.AVPlayerItemPlaybackStalled, object: myPlayer?.currentItem)
        NotificationCenter.default.addObserver(self, selector: #selector(playfailed(noti:)), name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: myPlayer?.currentItem)
        myPlayer?.currentItem?.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions.new, context: nil)
        myPlayer?.currentItem?.addObserver(self, forKeyPath: "playbackLikelyToKeepUp", options: [.new,.old], context: nil)
        myPlayer?.currentItem?.addObserver(self, forKeyPath: "timedMetadata", options: NSKeyValueObservingOptions.new, context: nil)
        myPlayer?.addObserver(self, forKeyPath: "rate", options: [.new,.old], context: nil)
        myPlayer?.addObserver(self, forKeyPath: "timeControlStatus", options: [.new,.old], context: nil)
        PlayerObserving = true*/
    }
    
    //MARK: - Logout Button
    @IBAction func logout_Pressed(sender:UIButton?){
        self.showLogoutAlert(message: "Are you sure?")
    }
    
    func showLogoutAlert(title:String = "MusicInxite",message:String?){
        
        var alert :UIAlertController!
        
        alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let yes = UIAlertAction(title: "Yes", style: .default, handler: { (action:UIAlertAction) in
            self.logoutSession()
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action:UIAlertAction) in
            
        })

        alert.addAction(yes)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: {
            
        })
    }
    
    func showErrorAlert(title:String = "MusicInxite",message:String?){
        
        var alert :UIAlertController!
        
        if let msg = message , msg.isEmpty{
            alert = UIAlertController(title: title, message: "", preferredStyle: .alert)
        }
        else{
            alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        }
        let ok = UIAlertAction(title: "Ok", style: .default, handler: { (action:UIAlertAction) in
            
        })
        alert.addAction(ok)
        self.present(alert, animated: true, completion: {
            
        })
    }
    
    func logoutSession()   {
        
        if appDelegate.is_reachable{
            
            APIManager.sharedInstance.logout(completion: { (dict, error) in
                if error == nil {
                    
                    let sessionManager = Alamofire.SessionManager.default
                    sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
                        dataTasks.forEach { $0.cancel() }
                        uploadTasks.forEach { $0.cancel() }
                        downloadTasks.forEach { $0.cancel() }
                    }
                    
                    UserDefaults.standard.set("NO", forKey: "IsLoggedIn")
                    UserDefaults.standard.removeObject(forKey: "current_Stream_ID")
                    self.pollingTimer?.invalidate()
                    DBAction.sharedInstance.DeleteAllData()
                    NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVAudioSessionInterruption, object: nil)

                    if self.PlayerObserving{
                        self.removeKVOmyplayer()
                    }
                    if self.queueObserving{
                        self.myQueuePlayer?.removeObserver(self, forKeyPath: "currentItem")
                        self.queueObserving = false
                    }
                    self.myPlayer?.pause()
                    self.myQueuePlayer?.pause()
                    
                    //remove FSPlayer object
                    self.fsPlayer?.pause()
                    self.fsPlayer = nil
                    
                    //remove or stop radioKit
                    self.radioKit.stopStream()
                    
                    if self.myPlayer != nil{
                        self.myPlayer = nil
                        self.myplayerItem = nil
                    }
                    self.myQueuePlayer = nil
                    
                    if self.presentingViewController != nil {
                        self.dismiss(animated: true) {
                            
                        }
                    }
                    else{
                        let LoginView: LoginVC = self.storyboard!.instantiateViewController(withIdentifier: "LoginViewController") as! LoginVC
                        self.present(LoginView, animated: true, completion:nil)
                    }
                }
                else    {
                    self.showErrorAlert(message: error?.localizedDescription)
                }
            })
        }
    }
    
    //MARK: Audio Streamer delegate method
    func audioController(_ audioController: FSAudioController!, allowPreloadingFor stream: FSAudioStream!) -> Bool {
        return true
    }
    func audioController(_ audioController: FSAudioController!, preloadStartedFor stream: FSAudioStream!) {
        
    }
    
    // MARK: - Memory Warning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("Memory warnings")
        // Dispose of any resources that can be recreated.
    }
}

extension PlayerVC: PropoverAlertDelegate,NetworkAlertDismissDelegate,AddSongsProtocol,RemoveSongsProtocol,ABAlertDismissDelegate,RBAlertDismissDelegate {
    func propoverViewDismissed()    {
        self.performSegue(withIdentifier: "NetworkPropover", sender: self)
    }
    
    func NetworkAlertDismiss() {
        self.performSegue(withIdentifier: "AddSongsAlert", sender: self)
    }
    
    func addSongsRequest()  {
        self.trackRequest(type: "more")
    }
    
    func removeSongsRequest()   {
        self.trackRequest(type: "removal")
    }
    
    func addButtonAlertDismiss()    {
        self.performSegue(withIdentifier: "RemoveSongsAlert", sender: self)
    }
    
    func removeButtonAlertDismiss() {
        self.performSegue(withIdentifier: "LogoutPropover", sender: self)
    }
}
