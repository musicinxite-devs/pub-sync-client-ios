//
//  PropoverAlertVC.swift
//  MusicInxite
//
//  Created by Pranav Panchal on 03/02/17.
//  Copyright © 2017 Kishor Lodhia. All rights reserved.
//

import UIKit

protocol PropoverAlertDelegate {
    func propoverViewDismissed()
}

class PropoverAlertVC: UIViewController,UIPopoverPresentationControllerDelegate {
    
    //MARK: - Delegate
    var delegate:PropoverAlertDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func ok_pressed(sender:UIButton?)   {
        self.dismiss(animated: false) {
            self.delegate?.propoverViewDismissed()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller
    }*/
}
